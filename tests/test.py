import os
import sys
import unittest
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from numlet.core import Formato, Numlet, Region


class TestNumlet(unittest.TestCase):
    def test_unico(self):
        nl = Numlet()
        texto = "dos pesos 00/100"
        numero = 2
        self.assertEqual(nl.convertir(numero), texto)

    def test_decenas(self):
        nl = Numlet()
        texto = "treinta y dos pesos 00/100"
        numero = 32
        self.assertEqual(nl.convertir(numero), texto)

    def test_centenas(self):
        nl = Numlet()
        texto = "ciento cincuenta y cuatro pesos 00/100"
        numero = 154
        self.assertEqual(nl.convertir(numero), texto)

    def test_millares(self):
        nl = Numlet()
        texto = "tres mil ochocientos veintiun pesos 00/100"
        numero = 3821
        self.assertEqual(nl.convertir(numero), texto)

    def test_decenas_millar(self):
        nl = Numlet()
        texto = "once mil ciento un pesos 00/100"
        numero = 11101
        self.assertEqual(nl.convertir(numero), texto)

    def test_centenas_millar(self):
        nl = Numlet()
        texto = "novecientos noventa y nueve mil novecientos noventa y nueve " \
                "pesos 00/100"
        numero = 999999
        self.assertEqual(nl.convertir(numero), texto)

    def test_cien(self):
        nl = Numlet()
        texto = "cien pesos 00/100"
        numero = 100
        self.assertEqual(nl.convertir(numero), texto)

    def test_un_peso(self):
        nl = Numlet()
        texto = "un peso 00/100"
        numero = 1
        self.assertEqual(nl.convertir(numero), texto)

    def test_max_not_fail(self):
        nl = Numlet()
        failed = False
        try:
            nl.convertir(nl.max())
        except ValueError:
            failed = True
        self.assertFalse(failed)

    def test_max_plus_one_fail(self):
        nl = Numlet()
        failed = False
        try:
            nl.convertir(nl.max() + 1)
        except ValueError:
            failed = True
        self.assertTrue(failed)

    def test_monedas(self):
        numero = 10

        nl = Numlet(Region.ARGENTINA)
        self.assertEqual(nl.convertir(numero), "diez pesos 00/100")
        nl = Numlet(Region.ESPANA)
        self.assertEqual(nl.convertir(numero), "diez euros 00/100")
        nl = Numlet(Region.MEXICO)
        self.assertEqual(nl.convertir(numero), "diez pesos 00/100")
        nl = Numlet(Region.VENEZUELA)
        self.assertEqual(nl.convertir(numero), "diez bolívares 00/100")


if __name__ == '__main__':
    unittest.main()
