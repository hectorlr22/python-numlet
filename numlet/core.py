"""
Módulo para convertir números a letras.
"""
import decimal as _d
import math
from enum import auto, Enum
from numlet import config


class Region(Enum):
    ARGENTINA = 'es-ar'
    ESPANA = 'es-es'
    MEXICO = 'es-mx'
    VENEZUELA = 'es-ve'


class Numlet(object):
    """
    Clase para manejar la conversión de números principalmente monetarios a
    letras, es muy útil cuando se desea imprmir los importes en los tickets de
    venta.

    NOTA: Llamar al método "max()" para saber cual es el número máximo
    soportado.
    """
    _MSG_ERROR = "Error de lógica"

    def __init__(self, region=None):
        self.region = region if region else Region.MEXICO

    def _es_unico(self, numero):
        for num, val in config.NUMEROS:
            if numero == num:
                return True, val
        return False, ""

    def _decimal(self, decimal):
        x, cents = _d.Decimal(decimal), _d.Decimal(".01")
        money = x.quantize(cents, _d.ROUND_HALF_UP)
        e, d = str(money).split(".")
        return d

    def _decenas(self, num):
        es_unico, val = self._es_unico(num)
        if es_unico:
            return val

        num_str = str(num)
        dec, uni = list(num_str)

        dec = int(dec) * 10
        uni = int(uni) * 1

        dec_unica, dec = self._es_unico(dec)
        uni_unica, uni = self._es_unico(uni)

        if not dec_unica or not uni_unica:
            raise ValueError(self._MSG_ERROR)

        return "{} y {}".format(dec, uni)

    def _centenas(self, num):
        if num < 100:
            return self._decenas(num)

        if num == 100:
            return "cien"

        es_unico, val = self._es_unico(num)
        if es_unico:
            return val

        num_str = str(num)
        cen, resto = num_str[0], num_str[1:]
        cen = int(cen) * 100
        cen_unica, cen = self._es_unico(cen)

        if not cen_unica:
            raise ValueError(self._MSG_ERROR)

        return "{} {}".format(cen, self._decenas(int(resto)))

    def _millares(self, num):
        es_unico, val = self._es_unico(num)
        if es_unico:
            return val

        numstr, long = str(num), len(str(num))
        part_a = ""
        n = long - 3
        mill, resto = numstr[0:n], numstr[n:]

        if long < 4:
            return self._centenas(num)

        if long == 4:
            # Millares
            es_unico, part_a = self._es_unico(int(mill))
            if not es_unico:
                raise ValueError(self._MSG_ERROR)
        elif long == 5:
            # Decenas de millar
            part_a = self._decenas(int(mill))
        elif long == 6:
            # Centenas de millar
            part_a = self._centenas(int(mill))

        if int(resto) == 0:
            resto = ""
        else:
            resto = self._centenas(int(resto))

        return "{} mil{}".format(
            part_a,
            " " + resto if len(resto) > 0 else resto
        )

    def _millones(self, num):
        numstr = str(num)
        long = len(numstr)

        if long < 7:
            return self._millares(num)

        es_unico, val = self._es_unico(num)
        if es_unico:
            return "un {}".format(val)

        n = long - 6
        part_a, resto = numstr[0:n], numstr[n:]

        millon_es = "millones"
        if int(part_a) == 1:
            # Definiendo si es plural o singular
            millon_es = "millón"

        part_a = self._millares(int(part_a))

        if int(resto) == 0:
            resto = "de"
        else:
            resto = self._millares(int(resto))

        return "{} {} {}".format(part_a, millon_es, resto)

    def _buscar(self, numero):
        numero_str = str(numero)
        longitud = len(numero_str)

        if longitud <= 12:
            return self._millones(numero)

        raise ValueError("La cantidad dada es mayor al valor máximo.")

    def _get_monedas(self):
        for cod, pais, sing, plur in config.CODIGOS_PAISES:
            if self.region.value == cod:
                return sing, plur
        raise ValueError("Región no encontrada")

    @staticmethod
    def max():
        """Regresa el número máximo que es posible convertir"""
        return 999999999999.99

    def convertir(self, numero, solo_texto=False):
        """
        Convierte un número entero o decimal a texto

        Parámetros:
        numero     -- (int, float) número a convertir
        formato    -- (numlet.core.Formato) formato del texto
        solo_texto -- (bool) define si solo mostrará el número convertido sin
                      considerar las monedas
        """
        decimal, entero = math.modf(numero)
        entero = int(entero)
        es_unico, val = self._es_unico(entero)
        singular, plural = self._get_monedas()
        if solo_texto:
            moneda = ""
        else:
            moneda = singular if entero == 1 else plural

        if es_unico:
            if val == "ciento":
                # Validando caso especial con el número 100
                val = "cien"
            elif val == "millón":
                # Validando caso especial con el millón
                val = "un millón de"
            return "{} {} {}/100".format(val, moneda, self._decimal(decimal))

        res = self._buscar(entero)

        resultado = "{} {} {}/100".format(res, moneda, self._decimal(decimal))
        return resultado
