import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

setup(
    name='numlet',
    version='0.1',
    packages=['numlet', 'tests'],
    description='Librería para convertir números a letras',
    long_description=README,
    author='Hector Lopez',
    author_email='hectorlr22@gmail.com',
    url='https://gitlab.com/hectorlr22/python-numlet',
    license='GPLv3',
    classifiers=[
        'Development Status :: Beta',
        'Intended Audience :: Developers',
        'Programming Language :: Python'
    ],
    keywords=['numeros', 'letras', 'convertir', 'traductor'],
)
