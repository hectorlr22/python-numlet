# Python Numlet

Numlet (Números a letras) es un paquete de python que convierte números enteros o flotantes a texto principalmente utilizado para representar cantidades monetarias en impresiones de tickets. 

## Empezando

Pendiente

### Prerrequisitos

Python 3.x

### Instalando

Pendiente

### Ejemplo
Primero hay que importar la clase Numlet que se encuentra dentro del módulo **core**

```
>>> from numlet.core import Numlet
```
Después instanciamos la clase Numlet
```
>>> nl = Numlet()
```
Llamamos al método **convertir** el cual se encarga de convertir un número dado a la cantidad en letras
```
>>> texto = nl.convertir(2536.6)
>>> print(texto)
dos mil quinientos treinta y seis pesos 60/100
```

## Corriendo pruebas

Para correr las pruebas unitarias, se debe ejecutar desde la terminal el módulo tests/test.py

```
python3 tests/test.py
```

## Deployment

Pendiente

## Autor

* **Hector Lopez** - *Fundador* - [Cuenta de Gitlab](https://gitlab.com/hectorlr22) 

## Licencia
Este proyecto está licenciado bajo la licencia GNU GENERAL PUBLIC LICENSE - ver el archivo [LICENSE](LICENSE.txt)
